function [rotated_psi] = rotate_psi(t,w,axis,psi,direction)

    if (direction == true)
        rotation = expm(1i*w*t*axis);
    elseif (direction == false)
        rotation = expm(-1i*w*t*axis) ;
    end

    rotated_psi = rotation * psi;

end
