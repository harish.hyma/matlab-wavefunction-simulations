% I think this means we are viewing the electron spin from the rotating
% frame, which is why the hamiltonian appears static. We are tranferring
% the time evolution of the rotation from the hamiltonian to the reference
% frame (spin axis)

close all;
clear all;
clc;

format short eng;

gamma_e = 2*pi*28e9;

h = 6.62606876e-34;
h_bar = h/(2*pi);

up_state = [1; 0];
down_state = [0; 1];

spin = 0.5;
angular_momentum = h_bar/2;

Sx = angular_momentum*[0, 1 ; 1, 0];
Sy = angular_momentum*[0, -i; i, 0];
Sz = angular_momentum*[1, 0; 0, -1];

B_z =  1;
B_xy = 20e-3;

w_0 = gamma_e*B_z;  % precession frequency around z-axis
w_1 = gamma_e*B_xy;  % Rabi frequency

dt = 1e-12;
period = ((2*pi)/(w_1));
time = 0:dt:period;

psi_t(:,1) = down_state;
p = 0.95:0.1:1.05;
p = [1];

for j = 1:length(p)

    w = p(j)*w_0; %Rotation frequency of lab frame around z

    H_w = spin * get_rotated_hamiltonian(w,w_0,w_1);

    [a,b] = get_sorted_eigens(H_w);

    Su = get_Su_matrix(0,0);

    for t = 2:length(time)

        psi_t(:,t) = time_evolve(dt,H_w,psi_t(:,t-1));

        % psi_r(:,k) = rotate_psi(time(k),w,Su,psi_t(:,k),false);

        exp_x(t) = get_expectation_value(psi_t(:,t),Sx)/angular_momentum;
        exp_y(t) = get_expectation_value(psi_t(:,t),Sy)/angular_momentum;
        exp_z(t) = get_expectation_value(psi_t(:,t),Sz)/angular_momentum;
        prob(t) = get_probability(down_state,psi_t(:,t));

        % exp_x(k) = get_expectation_value(psi_r(:,k),Sx);
        % exp_y(k) = get_expectation_value(psi_r(:,k),Sy);
        % exp_z(k) = get_expectation_value(psi_r(:,k),Sz);
        % prob(k) = get_probability(down_state,psi_r(:,k));

    end
end

figure
plot(time, prob)
xlim([0 period]);
xlabel('time(s)')
ylabel('Probability')

figure
plot(time, exp_x, time, exp_y, time, exp_z)
legend('exp_x','exp_y','exp_z');
xlim([0 period]);
xlabel('time(s)')
ylabel('Expectation')
