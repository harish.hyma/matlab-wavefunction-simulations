function [Su] = get_Su_matrix(theta, phi)
theta =  deg2rad(theta);
phi = deg2rad(phi);
Sx = [0, 1 ; 1, 0];
Sy = [0, -i; i, 0];
Sz = [1, 0; 0, -1];

Su = Sx*sin(theta)*cos(phi) + Sy*sin(theta)*sin(phi) + Sz*cos(theta);
end
