function [output_psi] = time_evolve(t, H_w, psi)
    exp_matrix = -1i*H_w*t;
    U_t = expm(exp_matrix);
    output_psi = U_t * psi;
end
