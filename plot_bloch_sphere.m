function plot_bloch_sphere(x,y,z)
  figure
  [sx,sy,sz] = sphere;
  p = surf(sx.*0.5,sy.*0.5,sz.*0.5);  % sphere centered at origin
  set(p,'facecolor','red','edgecolor','none');
%   set(p,'facecolor','red','edgecolor','none','facealpha',0.8);
  camlight; lighting gouraud;
%   alpha(0.2);
  hold on
  plot3(0.5*cos(0:0.01:2*pi),0.5*sin(0:0.01:2*pi),0*(0:0.01:2*pi),'color',[0.4 0.4 0.4]);
  plot3(0*(0:0.01:2*pi),0.5*cos(0:0.01:2*pi),0.5*sin(0:0.01:2*pi),'color',[0.4 0.4 0.4]);
  plot3(0.5*sin(0:0.01:2*pi),0*(0:0.01:2*pi),0.5*cos(0:0.01:2*pi),'color',[0.4 0.4 0.4]);
  plot3([-0.6 0.6], [0 0], [0 0],'color',[0.4 0.4 0.4]);
  plot3([0 0], [-0.6 0.6], [0 0],'color',[0.4 0.4 0.4]);
  plot3([0 0], [0 0], [-0.6 0.6],'color',[0.4 0.4 0.4]);
  axis([-0.6 0.6 -0.6 0.6 -0.6 0.6]);
  title('Time Evolution');
  xlabel('X');
  ylabel('Y');
  zlabel('Z');
  daspect([1 1 1]);
  plot3(x*0.5, y*0.5, z*0.5, 'k-')
end
