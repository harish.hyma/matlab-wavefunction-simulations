function [expectation_value] = get_expectation_value(psi,S)
expectation_value = psi'*S*psi;
end
