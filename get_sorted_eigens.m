function [eigenvalues,eigenvectors] = get_sorted_eigens(S)
[V,D] = eig(S);
[D,I] = sort(diag(D));
eigenvectors = V(:, I);
eigenvalues = D;
end