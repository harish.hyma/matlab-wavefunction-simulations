clear all;
close all;
clc;

h_bar = 6.62606876e-34/(2*pi); %Planck constant
m_e = 9.10938188e-31;  % electron mass
c = 3*10^8;

% w = (m_e * c^2)/h_bar

center_lambda = 1e-3; % center wavelength
k_0 = 2*pi*(1/center_lambda);

velocity = (h_bar*k_0)/m_e;

L = 20e-3;
resolution = 1e3;
x = linspace(0,L,resolution);

% end_time = 1e-3;
% time = linspace(0,end_time,1000);

dt = 0.01e-3;
time = 0:dt:10e-3;

max_states = 100; %should be even number
percentage = 0.1;
variance = 1e3;
[coeffs, states, k] = construct_wave_packet(max_states,k_0,percentage,variance,1);

max_states = max_states + 1;

psi_t = zeros(max_states,length(time));
phi_n = zeros(length(x),max_states);

H_w = zeros(max_states,max_states);

for j = 1:max_states
  H_w(j,j) = k(j);
end

for j = 1:max_states
    psi_t(:,1) = psi_t(:,1) + states(:,j);
end

for n = 1:max_states
  temp = coeffs(n)*exp(1i*k(n)*x);
  phi_n(:,n) = temp;
end

% plot_coeffs = plot(k,coeffs);

psi = zeros(length(x),length(time));

for n = 1:max_states
  psi_t(:,1) = psi_t(:,1) + states(n,n);
end

for n = 1:max_states
    psi(:,1) = psi(:,1) + psi_t(n,1) * phi_n(:,n);
end

for t = 2:length(time)
    
  psi_t(:,t) = time_evolve(time(t), H_w, psi_t(:,1));
  
  for n = 1:max_states
    psi(:,t) = psi(:,t) + psi_t(n,t) * phi_n(:,n);
  end
  
end

figure
h = plot(x,real(psi(:,1))); 
xlabel('x-position');
ylabel('Probability');
ylim([-10000 10000])

for t = 2:length(time)
%     pause(1e-2);
    set(h,'XData',x,'YData',real(psi(:,t))) ;
    drawnow;
end

