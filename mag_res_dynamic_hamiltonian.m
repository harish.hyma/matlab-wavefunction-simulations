% I think this means we are viewing the electron spin from the lab frame

clear all;
close all;
clc;

format short eng;

gamma_e = 2*pi*28e9;

spin = 0.5;

h = 6.62606876e-34;
h_bar = h/(2*pi);

angular_momentum = h_bar/2;

up_state = [1; 0];
down_state = [0; 1];

Sx = angular_momentum*[0, 1; 1, 0];
Sy = angular_momentum*[0, -i; i, 0];
Sz = angular_momentum*[1, 0; 0, -1];

B_z =  1;
B_xy = 20e-3;

w_0 = gamma_e*B_z  % precession angular frequency around z-axis
w_1 = gamma_e*B_xy; %Rabi angular frequency

dt = 1e-12;
period = (2*pi)/(w_1);
time = 0:dt:period;

psi_t = zeros(2,length(time));
psi_r = zeros(2,length(time));

psi_t(:,1) = down_state;
psi_r(:,1) = psi_t(:,1);

Su = get_Su_matrix(0,0);

w = w_0;

Bx = B_xy * cos(w*time);
By = B_xy * sin(w*time);
Bz = B_z;

  for t = 2:length(time)

      H_w = get_magnetic_hamiltonian(Bx(t),By(t),Bz,Sx,Sy,Sz,gamma_e)/h_bar;
      psi_t(:,t) = time_evolve(dt,H_w,psi_t(:,t-1));
     
%       psi_r(:,t) = rotate_psi(dt,w,spin*Su,psi_t(:,t-1),false);
% 
      exp_x(t) = get_expectation_value(psi_t(:,t),Sx)/angular_momentum;
      exp_y(t) = get_expectation_value(psi_t(:,t),Sy)/angular_momentum;
      exp_z(t) = get_expectation_value(psi_t(:,t),Sz)/angular_momentum;
      prob(t) = get_probability(up_state,psi_t(:,t));
% 
%       exp_x(t) = get_expectation_value(psi_r(:,t),Sx)/angular_momentum;
%       exp_y(t) = get_expectation_value(psi_r(:,t),Sy)/angular_momentum;
%       exp_z(t) = get_expectation_value(psi_r(:,t),Sz)/angular_momentum;
%       prob(t) = get_probability(up_state,psi_r(:,t));
  end

% figure
% plot(time,Bx,time,By)

figure
plot(time, prob)
xlim([0 period])
% set(p,'linewidth', 2)
xlabel('time(s)')
ylabel('Probability')

figure
plot(time, exp_x, time, exp_y, time, exp_z)
legend('exp_x','exp_y','exp_z');
xlim([0 period])
xlabel('time(s)')
ylabel('Expectation')

plot_bloch_sphere(exp_x,exp_y,exp_z)
