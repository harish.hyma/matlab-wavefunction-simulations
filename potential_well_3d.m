close all
clear all
clc

h_bar = 6.62606876e-34 / (2*pi); %Planck constant 
m_e = 9.10938188e-31;  % electron mass
k_B = 1.38e-23;
Tk = 0.01;
L = 1e-6;

resolution = 45;
x = linspace(0,L,resolution);
y = x;
Li = length(x);

dt = 10e-12;
time = 0:dt:10e-9; % time range
Ti = length(time);

n = 1;
Z = 0;

% beta =  1/(k_B*Tk);
% c = ((pi^2)*(h_bar^2))/(2*m_e*a^2);

% max_states = 4;

while true
    k = pi/L;
    E_n = 2*(n^2)*((k^2) * h_bar^2)/(2*m_e);    
    constant = exp(-E_n/(k_B*Tk));
    Z = Z + constant;

    P_n = constant/Z;
    
    if (P_n <= 0.01)
        break;
    end
    
    n = n + 1;
end
% n = 2;
max_states = n;

H = zeros(max_states,max_states);
states = construct_states(max_states);

phi_n = zeros(Li, Li, n);
psi_t = zeros(max_states,Ti);
psi_0 = zeros(max_states, 1);
psi= zeros(Li, Li, Ti);

E_n = zeros(max_states,max_states);
for a = 1:max_states
        E_n(a,a) = (2*a^2)*(pi^2 * h_bar^2)/(2*m_e*L^2);  
end

H_w = E_n/h_bar;

for j = 1:length(x)
    for k = 1:length(y)
        for a = 1:max_states
            phi_n(j,k,a) = (2/L)*sin(x(j)*a*pi/L)*sin(y(k)*a*pi/L);
        end
    end
end

% psi_0 = phi_n(:,:,1) + phi_n(:,:,2);
% psi_0 = phi_n(:,:,1) + phi_n(:,:,4);


% psi_0 = states(:,1) + states(:,2) + states(:,3) + states(:,4);

for k = 1:max_states
    psi_0(:) = psi_0(:) + states(:,k);
end

psi_t(:,1) = time_evolve(0, H_w, psi_0);

for t_ = 2:length(time)
    
    psi_t(:,t_) = time_evolve(dt, H_w, psi_t(:,t_-1));
    
%     total_prob(t_) = get_probability(psi_t(:,t_),psi_t(:,t_));
    
    total_psi = zeros(Li,Li,Ti);
    
    
    for k = 1:max_states
        total_psi(:,:,t_) = total_psi(:,:,t_) + psi_t(k,t_) * phi_n(:,:,k);
    end

%     total_prob_xy = zeros(Li,Li,Ti);
%     for k = 1:max_states
%         total_prob_xy(:,:,t_) = total_prob_xy(:,:,t_) + total_prob(t_) * phi_n(:,:,k);
%     end
    
    psi(:,:,t_) = total_psi(:,:,t_);
    
%     for k = 1:Li
%         for j = 1:Li
%             psi(k,:,t_) = total_psi(:,:,t_);
%         end
%     end
%    
%     total = trapz(y,trapz(x,(abs(psi(:,:,t_))).^2,2));
%     psi(:,:, t_) = psi(:,:,t_)/total;

    surf(x,y,10e-13*(abs(psi(:,:,t_))).^2);
%     lol = get_probability(psi,psi);
%     surf(x,y,total_prob_xy(:,:,t_));
    zlim([0,20]);
    xlabel('x-position');
    ylabel('y-position');
    zlabel('Probability');
    drawnow;
end

% imagesc(psi)
% drawnow;