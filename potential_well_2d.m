close all
clear all
clc

h_bar = 6.62606876e-34 / (2*pi); %Planck constant 
m_e = 9.10938188e-31;  % electron mass
k_B = 1.38e-23;
T = 0.005;
a = 1e-6;

n = 1;
Z = 0;

beta =  1/(k_B*T);
c = ((pi^2)*(h_bar^2))/(2*m_e*a^2);


while true
    k_n(n) = (n*pi)/a;
    E_n(n) = ((k_n(n)^2) * h_bar^2)/(2*m_e);    
    constant = exp(-E_n(n)/(k_B*T))
%     constant_(n) = exp(-n^2 * c*beta)
    Z = Z + constant

    P_n(n) = constant/Z;
    
    if (P_n(n) <= 0.01)
        break;
    end
    
    n = n + 1;
end

% Z_lol = sqrt(pi/(c*beta))

dt = 10e-12;    % time resolution
T_max = 100e-9;   % end time interval

H = zeros(n,n);
states = zeros(n,n);

x = linspace(0,a,500);

for k = 1:n
    H_w(k,k) = E_n(k)/h_bar;
    states(k,k) = 1; 
 
    psi_x(k,:) = sqrt(2/a)*sin(k_n(k)*x); 
end

psi_0 = zeros(n,n);

% for k = 1:n
% psi_0 = psi_0 + sqrt(P_n(k))*states(:,k);
% end

for k = 1:n
psi_0 = psi_0 + states(:,k);
end

t = 0:dt:T_max;
% figure
for t_k = 1:length(t)
    
    U_t = expm(-1i*H_w*t(t_k));
    psi_t = U_t * psi_0;
    
    total_states = 0;
    
    for k = 1:n
        total_states = total_states + psi_t(k,1)*psi_x(k,:)'; 
    end
    
    psi(:,t_k) = abs(total_states).^2;
    psi(:,t_k) = psi(:,t_k)/sum(psi(:,t_k));

    plot(x,psi(:,t_k))
    ylim([0,0.02]);
    xlabel('Position');
    ylabel('Probability');
    drawnow;
end

% imagesc(psi)
% drawnow;