function [H] = get_magnetic_hamiltonian(Bx,By,Bz,Sx,Sy,Sz,gamma_e)
    H = (gamma_e*(Bx * Sx + By * Sy + Bz * Sz));
end
