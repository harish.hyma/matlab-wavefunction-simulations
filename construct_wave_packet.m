function [packet_coeffs,states,k] = construct_wave_packet(max_states,k_0,p,variance,packet_type)

states = construct_states(max_states+1);
k_d = p/100 * k_0;
k_range = -max_states/2:1:max_states/2;
k_extra = k_d*k_range;
k = k_0 + k_extra;

% k = k_0 + k_extra

sigma = variance;
% packet_coeffs= normpdf (x, k_0, sigma)
% sig = k_d/(2*sqrt(2*log(2))); %variance of our Gaussian envelope for a FWHM = Delta_k
% sig = 0.001;

  if (packet_type == 1)
    packet_coeffs = normpdf(k, k_0, sigma);
    % packet_coeffs = (1/(sqrt(2*pi)*sig))*exp(-(1/2)*(k-k_0).^2/sig^2); %This is a Gaussian envelope function for the wave function
  elseif (packet_type == 2)
    packet_coeffs = (1/pi)*((k_d/2)./((k_d/2)^2+(k-k_0).^2)); %This is a Lorenzian wave envelope
  elseif (packet_type == 3)
    packet_coeffs = rectangularPulse(-k_d/2, k_d/2, k-k_0); % This is a rectangular function
  end
packet_coeffs = packet_coeffs/max(packet_coeffs);
end
