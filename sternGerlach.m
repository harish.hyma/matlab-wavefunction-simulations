function [psi_out_up, psi_out_down, exp_val, P_out_up ,P_out_down] = sternGerlach(psi_in, theta, phi)

Su = get_Su_matrix(theta,phi);
exp_val = get_expectation_value (psi_in, Su);

V = get_sorted_eigenvectors(Su);

P_out_up = get_probability(V(:,2),psi_in);
P_out_down = get_probability(V(:,1),psi_in);

psi_out_up = V(:,2);
psi_out_down = V(:,1);
end
