Matlab code to simulate various wave-functions using matrix mechanics implementation of quantum mechanics.
The code should be mostly compatible with Octave. If not, please inform me through email or submit a bug report and I will try to make it compatible with Octave!
Octave compatibility is very important to me personally because unfortunately not everyone has access to Matlab.
Please note, there could be some bits of code in this repo that was not written by me. I will however update the code at some point with the respected authors once I confirm whom it belongs to!

I uploaded all the code in a rush and so there is much left to be desired! 
Rest assured that I am working on it ;) One of my first tasks on the to-do list is to update bits of the code and organise them into folders.


I thank my supervisors, Dr Arne Laucht and Prof Andrea Morello, for making this possible and teaching me everything I currently know. 
I also thank the wonderful team of the FQT group at CQC2T for some of the lectures and fundamental insights.