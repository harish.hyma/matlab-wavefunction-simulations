close all;
clear all;
clc;

up_state = [1; 0];
down_state = [0; 1];

% angle of measurement
theta = [0];
phi = [360];

% Define initial PSI as combination of two states
theta_psi = deg2rad(0);
phi_psi = deg2rad(0);
alpha = cos(theta_psi/2)*exp(-i*phi_psi/2);
beta = sin(theta_psi/2)*exp(i*phi_psi/2);
psi_in = (alpha)*up_state + (beta)*down_state;

for k = 1:length(theta)
[psi_out_up, psi_out_down, exp_val, P_up, P_down] = sternGerlach(psi_in, theta(k), phi(k))
psi_in = psi_out_up; %choose what psi state we let back into a second measurement
end

% Plot sphere with probabilty density
% Prepare initial PSI as combination of two states

theta = deg2rad(linspace(0,180,15));
phi = deg2rad(linspace(0,360,15));

theta_psi = deg2rad(0);
phi_psi = deg2rad(0);

alpha = cos(theta_psi/2)*exp(-i*phi_psi/2)
beta = sin(theta_psi/2)*exp(i*phi_psi/2)
psi_in = (alpha)*up_state + (beta)*down_state;
psi_in = down_state;

for k = 1:length(theta)
for j = 1:length(phi)
[psi_out_up, psi_out_down, exp_val(k,j), P_up(k,j), P_down(k,j)] = sternGerlach(psi_in, theta(k), phi(j));
end
end

[theta,phi] = meshgrid(theta,phi);
figure
surf(rad2deg(theta),rad2deg(phi),exp_val','FaceColor','interp','EdgeColor','none');
xlabel('Theta');
ylabel('Phi');
[x,y,z] = sph2cart(theta,phi,0.5);


figure
surf(x,y,z,exp_val,'FaceColor','flat','EdgeColor','none');
