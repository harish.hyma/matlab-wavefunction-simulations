function [probability] = get_probability(state, psi)
probability = (abs(state'*psi)).^2;
end
