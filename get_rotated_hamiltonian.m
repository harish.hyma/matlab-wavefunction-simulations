function [H_w] = get_rotated_hamiltonian(w,w_0,w_1)
    w_d = w-w_0;
    H_w = [w_d, w_1; w_1,-w_d];
end
